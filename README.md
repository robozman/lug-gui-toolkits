# The Qt Framework
## A Presentation About Backup Strategies for LUG

**Author:** [Robby Zampino](https://robby.ohea.xyz)

## Topics Covered

* What is Qt?
* The Qt Widgets Paradigm
* The Qt Quick / QML Paradigm
* The Qt development model?
* Qt vs KDE?
* History of Qt?
* Other Notes

## Additional Resources

* [The QT Project](https://www.qt.io/)
* [Qt Widgets Documentation](https://doc.qt.io/qt-5/qtwidgets-index.html)
* [Qt Quick / QML Documentation](https://doc.qt.io/qt-5/qtqml-index.html)
